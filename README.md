Jenkins pipeline shared library to run Terraform
====

This shared library is capable to run Terraform command line to prepare and build
the environment using remote tfstate configuration and alike.
