package net.clara.br.jenkins.terraform

import groovy.text.GStringTemplateEngine

/**
 * Deal with the terraform environment and command lines.
 */
class TerraformRunner implements Serializable {
  // Default Terraform commands
  static final String ACTION_PLAN = 'plan'
  static final String ACTION_PLAN_DESTROY = 'plan -destroy'
  static final String ACTION_APPLY = 'apply'
  static final String ACTION_DESTROY = 'destroy -force'

  // Stores the current config
  private def config

  // Point to the current Jenkisn pipeline steps object instanciation.
  private def steps

  // The path where the Terraform binary is stored on file system.
  private String path

  // A rendered options to run the Terraform final command line.
  private String terraformOptions

  /**
   * Get the Terraform infrastructure as a code blueprint via Git.
   *
   */
  private void cloneRepository() {
    this.steps.sh("mkdir -p ${this.config.codeDirectory}")
    this.steps.dir(this.config.codeDirectory) {
      this.steps.git([
        url: this.config.repositoryUrl,
        branch: this.config.repositoryBranch,
        credentialsId: this.config.repositoryCredential
      ])
    }
  }

  /**
   * Setup where the Terraform binary resides on file system via Jenkins tool
   * configuration.
   *
   * This relies on Terraform plugin being installed on Jenkins.
   *
   */
  private void setupPath() {
    this.path = "${this.steps.tool(this.config.tool)}/terraform"
  }

  /**
   * Execute the Terraform command line with correct environment variables,
   * wrapped on Jenkins ansi color plugin and inside the directory where git
   * fetch the code.
   *
   * Note that, this is assuming you are running this inside an EC2 with proper
   * roles and does not uses AWS credentials as parameters.
   *
   * @param: action The Terraform action (plan, apply, etc.) to execute.
   * @param: options The options to append to the command line.
   */
  private void executeTerraformCommandLine(String action, String options = '') {
    this.steps.dir(this.config.codeDirectory) {
      this.steps.echo("Executing command: '${this.path} ${action} ${options}'")
      this.steps.withEnv([
        "AWS_DEFAULT_REGION=${this.config.awsRegion}",
        "AWS_REGION=${this.config.awsRegion}"
      ]) {
        this.steps.ansiColor() {
          this.steps.sh("${this.path} ${action} ${options}")
        }
      }
    }
  }

  /**
   * Configure the remote state of Terraform on a S3 bucket.
   *
   * @todo: implement multiples options for remote config.
   */
  private void remoteConfig() {
    this.executeTerraformCommandLine(
      'remote config',
      "-backend s3 -backend-config \"bucket=${this.config.stateBucket}\" -backend-config \"key=${this.config.stateBucketKey}\" -backend-config \"region=${this.config.stateAwsRegion}\""
    )
  }

  /**
   * Fetch all Terraform blurprint dependencies.
   *
   */
  private void getDependencies() {
    this.executeTerraformCommandLine('get')
  }

  /**
   * Build the options for Terraform command line based on Groovy templates
   * using the values passed on config for template body and binding variables.
   *
   */
  private void createTerraformRunOptions() {
    def engine = new GStringTemplateEngine()
    def renderedTemplate = engine.createTemplate(config.variablesTemplate).make(config.variablesTemplateBinding).toString()
    this.terraformOptions = '-var ' + renderedTemplate.split("\n").join(' ').replaceAll(' ', ' -var ')
  }

  /**
   * Constructor
   *
   * @param: steps The current Jenkins pipeline steps object instance.
   */
  public TerraformRunner(steps) {
    this.steps = steps
  }

  /**
   * receive the config object in hashmap format.
   *
   @ param config The config. This expect some values as the keys:
   * - tool: The name of the tool on terraform Jenkins plugin to use.
   * - codeDirectory: The directory to be created to store the Terraform blueprint code.
   * - repositoryUrl: The git repository URL to fetch the Terraform blueprint code.
   * - repositoryBranch: The git repository branch to fetch the Terraform blueprint code.
   * - repositoryCredential: The Jenkins SSH key credential name to use for fetch the code from git.
   * - awsRegion: The AWS region to use to deploy the infrastructure elements.
   * - stateBucket: The S3 bucket to store the Terraform state.
   * - stateBucketKey: The key (path) on S3 bucket to store the Terraform state.
   * - stateBucketRegion: The region of S3 bucket to store the Terraform state.
   * - variablesTemplate: A multi line text with template interpolation variables.
   * - variablesTemplateBinding: The variables for binding on the template.
   */
  public void setConfig(def config) {
    this.config = config
  }

  /**
   * Retrieve the current config object.
   *
   * @return HashMap The current config.
   */
  public String getConfig() {
    return this.config
  }

  /**
   * Execute all steps necessary to run Terraform.
   */
  public void prepareForRun() {
    this.cloneRepository()
    this.setupPath()
    this.remoteConfig()
    this.getDependencies()
    this.createTerraformRunOptions()
  }

  /**
   * Run the final terraform command line to change the infrastructure.
   *
   * @param: run The Terraform comand action (paln, appy, etc.)
   */
  public void run(String action) {
    this.executeTerraformCommandLine(action, this.terraformOptions)
  }

  /**
   * Retrieve the current tfstate in JSON object format.
   *
   * @return: The structure of the tfstate or null if the file does not exists
   */
  public def getState() {
    def json = null

    this.steps.dir(this.config.codeDirectory) {
      json = this.steps.readJSON([file: "${this.steps.pwd()}/.terraform/terraform.tfstate"])
    }

    return json
  }
}
